// VirmonDDemo.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <json\json.h>
#include <staticlink\json.h>

#include <Windows.h>

#include <string>
#include <fstream>
#include <iostream>
#include <streambuf>
#include <sstream>

#include "..\PrintTcp\PrintTcp.h"

using namespace std;

void TestSendJobToServer()
{
	//typedef DWORD(__stdcall * pFunction)(const char * host, const char * requestBuf, DWORD sizeRequestBuf);

	//HMODULE module = LoadLibrary(L"PrintTcp.dll");

	//pFunction p = (pFunction)GetProcAddress(module, "SendRawData");

	// c:\\3.data
	auto f = fopen("c:\\_M1213nf 2.data", "rb");
	//auto f = fopen("E:\\temp\\00008.SPL", "rb");

	HANDLE hPrinter;
	OpenTcpPrinter(L"192.168.1.15", &hPrinter);

	int readed;
	const int size = 1024 * 1024;
	char * buf = (char *)malloc(size);

	while ((readed = fread(buf, sizeof(char), size, f)) != 0)
	{
		printf("send %d\r\n", readed);
		//DWORD value = p("192.168.1.15", buf, readed);
		WriteTcpPrinter(hPrinter, buf, readed, NULL, 0, NULL);
	}

	CloseTcpPrinter(hPrinter);
}

int _tmain(int argc, _TCHAR* argv[])
{
	TestSendJobToServer();

	system("pause");
	return 0;
}

