﻿#include "stdafx.h"

#include <Winsock2.h>
#pragma comment(lib, "ws2_32.lib")

#include "PrintTcp.h"

ULONG inet_addr_w(IN const WCHAR * cp)
{
	WCHAR cp0[128] = { 0 };
	char  cp1[128] = { 0 };
	int   iLen = 0;
	int   idx = 0;

	iLen = wcslen(cp);
	wcscpy(cp0, cp);
	for (idx = 0; idx<iLen; idx++)
	{
		cp1[idx] = 0xFF & ((char)cp0[idx]);
	}
	return inet_addr(cp1);
}

typedef struct _MyTcpPrinter
{
	char s[3];
	SOCKET socket;
}MyTcpPrinter;

DWORD __stdcall OpenTcpPrinter(LPCWSTR host, HANDLE * h){

	if (host == NULL)
	{
		_PRINT_ERROR(L"host is null, return -1");
		return -1;
	}

	_PRINT_INFO(L"host: %ws", host);

	// TCP
	const int port = 9100;

	WSADATA ws;
	if (0 != WSAStartup(MAKEWORD(2, 2), &ws))
	{
		return -1;
	}

	SOCKET client_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == client_socket)
	{
		return -1;
	}
	_PRINT_INFO(L"socket: %d", client_socket);

	int rsTimeout = 5000;
	setsockopt(client_socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&rsTimeout, sizeof(rsTimeout));
	setsockopt(client_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&rsTimeout, sizeof(rsTimeout));

	unsigned long ul = 1;
	ioctlsocket(client_socket, FIONBIO, (unsigned long *)&ul);

	sockaddr_in server_addr;
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr_w(host);
	server_addr.sin_port = htons(port);
	memset(server_addr.sin_zero, 0x00, 8);

	connect(client_socket, (sockaddr*)&server_addr, sizeof(server_addr));

	//select
	struct timeval timeout;
	fd_set	r;
	FD_ZERO(&r);
	FD_SET(client_socket, &r);
	timeout.tv_sec = 30;
	timeout.tv_usec = 0;
	if (SOCKET_ERROR == select(0, &r, &r, 0, &timeout))
	{
		wchar_t errText[256] = { 0 };
		swprintf(errText, L"数据服务器连接失败，请检查ServerConfig.ini文件配置。(%d)", WSAGetLastError());
		::MessageBoxW(NULL, errText, L"通讯", 0);

		goto Err;
	}

	MyTcpPrinter * hP = new MyTcpPrinter();
	hP->s[0] = 'b';
	hP->s[1] = 'i';
	hP->s[2] = 's';
	hP->socket = client_socket;

	*h = hP;

	return ERROR_SUCCESS;

Err:
	_PRINT_ERROR(L"close socket"); 
	closesocket(client_socket);
	WSACleanup();

	return -1;
}

DWORD __stdcall WriteTcpPrinter(HANDLE h,
	const char * request_buf, DWORD request_buf_len,
	char * response_buf, DWORD response_buf_len, LPDWORD response_buf_ret)
{
	MyTcpPrinter * hP = (MyTcpPrinter *)h;
	if (hP->s[0] != 'b' ||
		hP->s[1] != 'i' ||
		hP->s[2] != 's')  return ERROR_INVALID_HANDLE;

	_PRINT_INFO(L"socket: %d", hP->socket);
	_PRINT_INFO(L"request_buf_len: %d", request_buf_len);

	DWORD retCode;
	SOCKET client_socket = hP->socket;

	memset(response_buf, 0, response_buf_len);

	while (SOCKET_ERROR == send(client_socket, request_buf, request_buf_len, 0))
	{
		retCode = WSAGetLastError();
		if (retCode == WSAEWOULDBLOCK)
		{
			_PRINT_INFO(L"send would block. (10035)");
			Sleep(500);
		}
		else
		{
			_PRINT_ERROR(L"send data error. %d", retCode);
			goto Err;
		}
	}

	// Don't shutdown here for samsung printer

	//if (SOCKET_ERROR == shutdown(client_socket, SD_SEND))
	//{
	//	retCode = WSAGetLastError();
	//	_PRINT_ERROR(L"shutdown error. %d", retCode);
	//	goto Err;
	//}

	//do
	//{
	//	int rec;

	//	if (response_buf == NULL || response_buf_len == 0)
	//	{
	//		char b[1024];
	//		rec = recv(client_socket, b, 1024, 0);
	//	}
	//	else
	//	{
	//		rec = recv(client_socket, response_buf, response_buf_len, 0);
	//	}

	//	if (rec == 0) {
	//		// If the connection has been gracefully closed, the return value is zero
	//		break;
	//	}
	//	else if (rec == SOCKET_ERROR) {
	//		retCode = WSAGetLastError();
	//		if (retCode == WSAEWOULDBLOCK)	{
	//			Sleep(1);
	//		}
	//		else {
	//			_PRINT_ERROR(L"recv data error. %d", retCode);
	//			goto Err;
	//		}
	//	}
	//	else {
	//		if (response_buf_ret != NULL) *response_buf_ret = rec;
	//		break;
	//	}
	//} while (true);

	return ERROR_SUCCESS;

Err:
	return retCode;
}

DWORD __stdcall CloseTcpPrinter(HANDLE h)
{
	MyTcpPrinter * hP = (MyTcpPrinter *)h;
	if (hP->s[0] != 'b' ||
		hP->s[1] != 'i' ||
		hP->s[2] != 's')  return ERROR_INVALID_HANDLE;

	// rosefinch
	SOCKET client_socket = hP->socket;

	if (SOCKET_ERROR == shutdown(client_socket, SD_SEND))
	{
		DWORD retCode = WSAGetLastError();
		_PRINT_ERROR(L"shutdown error. %d", retCode);
	}

	do
	{
		int rec;

		char b[1024];
		rec = recv(client_socket, b, 1024, 0);

		if (rec == 0) {
			// If the connection has been gracefully closed, the return value is zero
			break;
		}
		else if (rec == SOCKET_ERROR) {
			DWORD retCode = WSAGetLastError();
			if (retCode == WSAEWOULDBLOCK)	{
				Sleep(1);
			}
			else {
				_PRINT_ERROR(L"recv data error. %d", retCode);
				break;
			}
		}
	} while (true);

	delete hP;

	closesocket(client_socket);
	WSACleanup();

	return ERROR_SUCCESS;
}