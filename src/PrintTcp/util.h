/******************************************************************************
	文 件 名:		Util.cpp
	
	文件描述:		工具函数声明文件
	
	作　　者:		Zhang Yiqi
	
	编写日期:		2008.04
	
	版权声明:		北京鼎盾信息科技有限公司版权所有
******************************************************************************/
#pragma once

typedef long NTSTATUS;

#include <windows.h>
#include <winspool.h>

//#ifndef _M_X64
//#include <winsplp.h>
//#endif

#include <wchar.h>

//#include "bisprint.h"

//#ifndef _M_X64
//#include <winddiui.h>
//#endif

#include <string>


LPWSTR AllocSplStr(LPWSTR pStr);
LPVOID AllocSplMem(DWORD dwAlloc);
LPVOID ReallocSplMem(LPVOID pOldMem, 
                     DWORD cbOld, 
                     DWORD cbNew);


#define FreeSplMem( pMem )        (GlobalFree( pMem ) ? FALSE:TRUE)
#define FreeSplStr( lpStr )       ((lpStr) ? (GlobalFree(lpStr) ? FALSE:TRUE):TRUE)

std::wstring IP_ByteToString(const BYTE abyIP[4]);
//void ErrMessageBox(int iResult, const wchar_t *szMsg);
// void ErrMessageBox(int iResult, const wchar_t *szMsg, const int iReversedRetryCount = 0);
void ErrMessageBox(const wchar_t *szMsg);
void ErrMessageBox(const std::wstring &szMsg);

void InfoMessageBox(const wchar_t *szMsg);
void InfoMessageBox(const std::wstring &szMsg);

std::wstring GetErrMessage(int iResult, const wchar_t *szMsg, const int iReversedRetryCount = 0);

std::wstring GetDriverName(const wchar_t *szPrinterName);