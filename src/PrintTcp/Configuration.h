// encoding=gb2312
/* 修订履历：
 * 2010-06-09 Bug #6707 替换Bis-soft为VENDOR_NAME，以避免向OEM版引入隐藏Bug
 * 
 */
#pragma once

#include <string>
#include <Win32Util/Language/Text/Text.h>
#include <Win32Util/Win32/Win32Exception.h>
#include <Win32Util/Win32/filesystem.h>
#include <Win32Util/Win32/ini.h>
#include <ResourceDLL/ResourceFetcher.h>

using namespace std;
using namespace boost;

struct Configuration
{
private:
	Configuration()
	{
		using Language::Text::Join;

		wstring szIni = Join(Win32::FileSystem::SpecialFolders::GetCommonAppData(), L"\\PrintSystem\\Client\\Temp\\Config.ini");
		Win32::INI::ProfileReader objReader(szIni);

		bNeedAjustDeviceSize = objReader.GetBoolean(L"DpiAdjust", L"NeedAjustDeviceSize", false);
		fTolerancePercent = (float)objReader.GetInt(L"DpiAdjust", L"TolerancePercent", 10);
		fDpi = (float)objReader.GetInt(L"DpiAdjust", L"Dpi", 300);
		iBmpDpi = objReader.GetInt(L"BisDraw", L"Dpi", 300);
		iTransferMode = objReader.GetInt(L"SocketSetting", L"TransferMode", 1);
		// 不要修改此行代码的注释
		bEnableSecurityLevelAndPurpose = true; // select_version:#EnableSecurityLevelAndPurpose
	}

public:
	static Configuration GetConfig()
	{
		static Configuration objInstance;
		return objInstance;
	}
	bool bNeedAjustDeviceSize;
	float fTolerancePercent;
	float fDpi;
	int   iBmpDpi;
	bool bEnableSecurityLevelAndPurpose;
	int  iTransferMode;
};
