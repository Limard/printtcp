// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#define WINVER		0x0500
#define _WIN32_WINNT	0x0501
#define _WIN32_IE	0x0501
#define _RICHEDIT_VER	0x0200

#define PRINT_TCP_EXPORT

#pragma warning(disable : 4482)

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             //  从 Windows 头文件中排除极少使用的信息
// Windows 头文件:
#include <windows.h>

#define __DISPLAY_LOG_LEVEL__ 0
#include <DebugString.h>

//#define _USE_LOG 1
//#include <LogInterface/LogDefine.h>
#include <staticlink/Utility.h>

#include <staticlink/json.h>