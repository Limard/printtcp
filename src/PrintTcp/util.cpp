/******************************************************************************
	文 件 名:		Util.cpp
	
	文件描述:		工具函数实现文件
	
	作　　者:		Zhang Yiqi
	
	编写日期:		2008.04
	
	版权声明:		北京鼎盾信息科技有限公司版权所有
******************************************************************************/
#include "stdafx.h"

#include "util.h"
#include <Common/Const.h>
//#include <ResourceDLL/ResourceFetcher.h>
#include <boost/bind.hpp>
#include <Win32Util/Language/scope_guard.h>

#include <DebugString.h>

using namespace std;

// IP地址的字节数
const int IP_BYTE_COUNT = 4;

// 字符表示的IP地址的最大长度
const int MAX_IP_CHARS  = 15;

/******************************************************************************
描　述:	
		重新分配内存（相当于C运行库的realloc）

参　数:		
		pOldMem			[IN][LPVOID]	旧的内存空间
		cbOld			[IN][DWORD]		旧的内存空间的大小
		cbNew			[IN][DWORD]		新的内存空间的大小



返回值:		
		重新分配出的内存指针

作　者:	Zhang Yiqi
日　期:	2008.4
******************************************************************************/
LPVOID ReallocSplMem(LPVOID pOldMem, DWORD cbOld, DWORD cbNew)
{
    LPVOID pNewMem;

    pNewMem = AllocSplMem(cbNew);

    if (pOldMem && pNewMem) 
	{
        if (cbOld) 
		{
            CopyMemory( pNewMem, pOldMem, min(cbNew, cbOld));
        }
        FreeSplMem(pOldMem);
    }
    return pNewMem;
}

/******************************************************************************
描　述:	
		这个函数分配足够的内存以保存指定的字符串，并将该字符串复制到这块内存中

参　数:		
		pStr			[IN][LPWSTR]	需要被保存的字符串

返回值:		
		重新分配出的内存指针

作　者:	Zhang Yiqi
日　期:	2008.4
******************************************************************************/
LPWSTR AllocSplStr(LPWSTR pStr)
{
    LPWSTR pMem  = NULL;
    DWORD  cbStr = 0;

    if (!pStr) 
	{
        return NULL;
    }

    cbStr = (DWORD)wcslen(pStr)*sizeof(WCHAR) + sizeof(WCHAR);

    if (pMem = (LPWSTR)AllocSplMem( cbStr )) 
	{
        CopyMemory( pMem, pStr, cbStr );
    }
    return pMem;
}

/******************************************************************************
描　述:	
		分配指定大小的内存，并将这部分内存清零

参　数:		
		pStr			[IN][LPWSTR]	需要被保存的字符串

返回值:		
		重新分配出的内存指针

作　者:	Zhang Yiqi
日　期:	2008.4
******************************************************************************/
LPVOID AllocSplMem(DWORD dwAlloc)
{
    PVOID pvMemory;

    pvMemory = GlobalAlloc(GMEM_FIXED, dwAlloc);

    if( pvMemory ){
        ZeroMemory( pvMemory, dwAlloc );
    }

    return pvMemory;
}

/******************************************************************************
描　述:	
		将BYTE数组表示的IP地址转换为字符串表示

参　数:		
		abyIP			[IN][BYTE[IP_BYTE_COUNT]]	需要被保存的字符串

返回值:		
		重新分配出的内存指针

作　者:	Zhang Yiqi
日　期:	2008.4
******************************************************************************/
wstring IP_ByteToString(const BYTE abyIP[IP_BYTE_COUNT])
{
	// 提出常量
	wchar_t szIP[MAX_IP_CHARS + 1];
	wsprintf(szIP, L"%d.%d.%d.%d", abyIP[0], abyIP[1], abyIP[2], abyIP[3]);

	//_PRINT_DEBUG(szIP);

	return wstring(szIP);
}

wstring GetErrMessage(int iResult, const wchar_t *szMsg, const int iReversedRetryCount)
{
	//const wchar_t *szTitle = L"鼎盾打印安全解决方案";
	wstring szText = szMsg;

	switch(iResult)
	{
	//case PSERR_INITIALIZING:		// 打印服务器处于初始状态
	case PSERR_LOCAL_COM_INVALID:	// 本地COM对象无效
	//case PSERR_NETWORK_INVALID:		// 连接不到指定的打印服务器
	case PSERR_SVR_COM_INVALID:		// 打印服务器DCOM无效
		szText.append(L"错误原因：COM对象无效。");
		break;
	case PSERR_NO_AUTHORITY:		// 正版认证未通过
		szText.append(L"错误原因：正版验证未通过。");
		break;
	case PSERR_NEED_LICENSE:		// License数不足
		szText.append(L"错误原因：License数不足。");
		break;
	case PSERR_AUTHENTIC_FAILED:	// 用户认证不通过
		{
			wchar_t szMsgText[128] = {0};
			if(iReversedRetryCount != 0)
			{
				wsprintf(szMsgText, L"错误原因：密码不正确。您还能尝试%d次登陆。", iReversedRetryCount);
			}
			else
			{
				wsprintf(szMsgText, L"错误原因：用户认证未通过。");
			}
			szText.append(szMsgText);
		}
		break;
	//case PSERR_USER_ALEARY_EXIST:	// 打印用户已存在
	//	szText.append(L"错误原因：用户已存在。");
	//	break;
	//case PSERR_FULL_QUOTA:			// 用户数已满
	//	szText.append(L"错误原因：用户数已满。");
	//	break;
	case PSERR_USER_DISABLED:
		szText.append(L"错误原因：用户未激活。");
		break;
	case PSERR_SERVER_BUSY:
		szText.append(L"错误原因：操作失败。可能服务器忙。");
		break;
	case PSERR_DATA_NOT_EXISTS:
		szText.append(L"错误原因：用户不存在。");
		break;
	case PSERR_INVALID_PARAMETER:	// 错误的参数
		szText.append(L"错误原因：参数错误。");
		break;
	case PSERR_FAILED:
		szText.append(L"错误原因：内部处理错误。");
		break;
	//case PSERR_INVALID_PRINT_SETTING:
	//	szText.append(L"错误原因：非法的打印页面设置。");
	//	break;
	case PSERR_ACCOUNT_LOCKED:
		szText.append(L"错误原因：帐户已锁定，请联系管理员解锁。");
		break;
	default:
		szText.append(L"错误原因：未知错误。");
		break;
	}

	return szText;
}

// static void ErrMessageBox(int iResult, const wchar_t *szMsg, const int iReversedRetryCount)
// {
// 	wstring szText = GetErrMessage(iResult, szMsg, iReversedRetryCount);
// 	MessageBoxW(NULL, szText.c_str(), PRODUCT_NAME, MB_OK | MB_SERVICE_NOTIFICATION | MB_TOPMOST);
// }


void InfoMessageBox(const wchar_t *szMsg)
{
	//MessageBoxW(NULL, szMsg, CResourceDLL::PRODUCT_NAME(), MB_OK | MB_TOPMOST | MB_ICONINFORMATION);
	MessageBoxW(NULL, szMsg, L"", MB_OK | MB_TOPMOST | MB_ICONINFORMATION);
}

void InfoMessageBox(const std::wstring &szMsg)
{
	InfoMessageBox(szMsg.c_str());
}
 
 
void ErrMessageBox(const wchar_t *szMsg)
{
	//MessageBoxW(NULL, szMsg, CResourceDLL::PRODUCT_NAME(), MB_OK | MB_TOPMOST | MB_ICONERROR);
	MessageBoxW(NULL, szMsg, L"", MB_OK | MB_TOPMOST | MB_ICONERROR);
}

void ErrMessageBox(const std::wstring &szMsg)
{
	ErrMessageBox(szMsg.c_str());
}


std::wstring GetDriverName(const wchar_t *szPrinterName)
{
	HANDLE hPrinter = NULL;
	wchar_t *pTempName = _wcsdup(szPrinterName);
	ON_SCOPE_EXIT(boost::bind<void>(free, pTempName));

	BOOL bRet = OpenPrinter(pTempName, &hPrinter, NULL);
	if(!bRet || !hPrinter)
	{
		
	}
	ON_SCOPE_EXIT(boost::bind<BOOL>(ClosePrinter, hPrinter));

	DWORD dwNeed = 0;
	GetPrinter(hPrinter, 2, NULL, 0, &dwNeed);
	if(dwNeed != 0)
	{
		PRINTER_INFO_2 *pInfo2 = (PRINTER_INFO_2 *)malloc(dwNeed);
		if(GetPrinter(hPrinter, 2, (BYTE*)pInfo2, dwNeed, &dwNeed))
		{
			std::wstring szDriverName = std::wstring(pInfo2->pDriverName);
			free(pInfo2);
			return szDriverName;
		}
		else
		{
			
		}
	}
	return std::wstring();
}
