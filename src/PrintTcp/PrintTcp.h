﻿#pragma once

#include <Windows.h>

#ifndef PRINT_TCP_EXPORT
#pragma comment(lib, "PrintTcp.lib")
#endif

DWORD __stdcall OpenTcpPrinter(LPCWSTR host, HANDLE * h);

DWORD __stdcall WriteTcpPrinter(HANDLE h,
	const char * request_buf, DWORD request_buf_len,
	char * response_buf, DWORD response_buf_len, LPDWORD response_buf_ret);

DWORD __stdcall CloseTcpPrinter(HANDLE h);